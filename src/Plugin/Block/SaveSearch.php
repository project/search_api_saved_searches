<?php

namespace Drupal\search_api_saved_searches\Plugin\Block;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Error;
use Drupal\search_api\Utility\QueryHelperInterface;
use Drupal\search_api_saved_searches\LoggerTrait;
use Drupal\search_api_saved_searches\SavedSearchTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Displays the "Save search" form in a block.
 *
 * @Block(
 *   id = "search_api_saved_searches",
 *   admin_label = @Translation("Save search"),
 *   category = @Translation("Forms"),
 * )
 */
class SaveSearch extends BlockBase implements ContainerFactoryPluginInterface {

  use LoggerTrait;

  /**
   * The entity type manager.
   */
  protected ?EntityTypeManagerInterface $entityTypeManager = NULL;

  /**
   * The form builder.
   */
  protected ?FormBuilderInterface $formBuilder = NULL;

  /**
   * The query helper.
   */
  protected ?QueryHelperInterface $queryHelper = NULL;

  /**
   * The request stack.
   */
  protected ?RequestStack $requestStack = NULL;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    $block = new static($configuration, $plugin_id, $plugin_definition);

    $block->setStringTranslation($container->get('string_translation'));
    $block->setEntityTypeManager($container->get('entity_type.manager'));
    $block->setFormBuilder($container->get('form_builder'));
    $block->setQueryHelper($container->get('search_api.query_helper'));
    $block->setRequestStack($container->get('request_stack'));
    $block->setLogger($container->get('logger.channel.search_api_saved_searches'));

    return $block;
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  public function getEntityTypeManager(): EntityTypeManagerInterface {
    return $this->entityTypeManager ?: \Drupal::entityTypeManager();
  }

  /**
   * Sets the entity type manager.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The new entity type manager.
   *
   * @return $this
   */
  public function setEntityTypeManager(EntityTypeManagerInterface $entity_type_manager): self {
    $this->entityTypeManager = $entity_type_manager;
    return $this;
  }

  /**
   * Retrieves the form builder.
   *
   * @return \Drupal\Core\Form\FormBuilderInterface
   *   The form builder.
   */
  public function getFormBuilder(): FormBuilderInterface {
    return $this->formBuilder ?: \Drupal::formBuilder();
  }

  /**
   * Sets the form builder.
   *
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The new form builder.
   *
   * @return $this
   */
  public function setFormBuilder(FormBuilderInterface $form_builder): self {
    $this->formBuilder = $form_builder;
    return $this;
  }

  /**
   * Retrieves the query helper.
   *
   * @return \Drupal\search_api\Utility\QueryHelperInterface
   *   The query helper.
   */
  public function getQueryHelper(): QueryHelperInterface {
    return $this->queryHelper ?: \Drupal::service('search_api.query_helper');
  }

  /**
   * Sets the query helper.
   *
   * @param \Drupal\search_api\Utility\QueryHelperInterface $query_helper
   *   The new query helper.
   *
   * @return $this
   */
  public function setQueryHelper(QueryHelperInterface $query_helper): self {
    $this->queryHelper = $query_helper;
    return $this;
  }

  /**
   * Retrieves the request stack.
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   The request stack.
   */
  public function getRequestStack(): RequestStack {
    return $this->requestStack ?: \Drupal::service('request_stack');
  }

  /**
   * Sets the request stack.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The new request stack.
   *
   * @return $this
   */
  public function setRequestStack(RequestStack $request_stack): self {
    $this->requestStack = $request_stack;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'type' => NULL,
    ];
  }

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Blocks that need to add form elements to the normal block configuration
   * form should implement this method.
   *
   * @param array $form
   *   The form definition array for the block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *   Thrown if the "Saved search type" entity storage could not be retrieved.
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $types = $this->getEntityTypeManager()
      ->getStorage('search_api_saved_search_type')
      ->loadMultiple();
    $type_options = [];
    foreach ($types as $type_id => $type) {
      $type_options[$type_id] = $type->label();
    }
    $form['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Saved search type'),
      '#description' => $this->t('The type/bundle of saved searches that should be created by this block.'),
      '#options' => $type_options,
      '#default_value' => $this->configuration['type'] ?: key($type_options),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['type'] = $form_state->getValue('type');
  }

  /**
   * {@inheritdoc}
   */
  public function access(AccountInterface $account, $return_as_object = FALSE): AccessResultInterface|bool {
    $access = parent::access($account, TRUE);

    $create_access = $this->getEntityTypeManager()
      ->getAccessControlHandler('search_api_saved_search')
      ->createAccess($this->configuration['type'], $account, [], TRUE);
    $access = $access->andIf($create_access);

    return $return_as_object ? $access : $access->isAllowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $cacheability = new CacheableMetadata();

    // @todo Move those checks to access()? Would mean access results can't be
    //   cached, though.
    $type = $this->getSavedSearchType();
    if (!$type) {
      try {
        $tags = $this->getEntityTypeManager()
          ->getDefinition('search_api_saved_search_type')
          ->getListCacheTags();
        $cacheability->addCacheTags($tags);
      }
      catch (PluginNotFoundException $e) {
        Error::logException($this->getLogger(), $e);
        $cacheability->setCacheMaxAge(0);
      }
      $cacheability->applyTo($build);
      return $build;
    }
    $cacheability->addCacheableDependency($type);
    if (!$type->status()) {
      $cacheability->applyTo($build);
      return $build;
    }

    // Since there is no cache context for "search query on this page", we can't
    // cache this block (unless building it didn't get this far).
    $cacheability->setCacheMaxAge(0);
    $cacheability->applyTo($build);
    $query = $type->getActiveQuery($this->getQueryHelper());
    if (!$query) {
      return $build;
    }

    $description = $type->getOption('description');
    if ($description) {
      $build['description']['#markup'] = Xss::filterAdmin($description);
    }

    $values = [
      'type' => $type->id(),
      'index_id' => $query->getIndex()->id(),
      'query' => $query,
      // Remember the page on which the search was created.
      'path' => $this->getCurrentPath(),
    ];
    try {
      $saved_search = $this->getEntityTypeManager()
        ->getStorage('search_api_saved_search')
        ->create($values);

      $form_object = $this->getEntityTypeManager()
        ->getFormObject('search_api_saved_search', 'create');
      $form_object->setEntity($saved_search);
      $build['form'] = $this->getFormBuilder()->getForm($form_object);
    }
    catch (PluginException $e) {
      Error::logException($this->getLogger(), $e);
      $this->messenger()->addError($this->t('Saving this search is not possible due to an internal error.'));
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies(): array {
    $dependencies = parent::calculateDependencies();

    $type = $this->getSavedSearchType();
    if ($type) {
      $dependencies['config'][] = $type->getConfigDependencyName();
    }

    return $dependencies;
  }

  /**
   * Loads the saved search type used for this block.
   *
   * @return \Drupal\search_api_saved_searches\SavedSearchTypeInterface|null
   *   The saved search type, or NULL if it couldn't be loaded.
   */
  protected function getSavedSearchType(): ?SavedSearchTypeInterface {
    if (!$this->configuration['type']) {
      return NULL;
    }
    try {
      /** @var \Drupal\search_api_saved_searches\SavedSearchTypeInterface $type */
      $type = $this->getEntityTypeManager()
        ->getStorage('search_api_saved_search_type')
        ->load($this->configuration['type']);
      return $type;
    }
    catch (PluginException $e) {
      Error::logException($this->getLogger(), $e);
      return NULL;
    }
  }

  /**
   * Retrieves a sanitized version of the current path.
   *
   * @return string
   *   The current path, relative to the Drupal installation.
   */
  protected function getCurrentPath(): string {
    // Get the current path.
    $path = $this->getRequestStack()->getCurrentRequest()->getRequestUri();

    // Remove the Drupal base path, if any.
    $base_path = rtrim(base_path(), '/');
    $base_path_length = strlen($base_path);
    if ($base_path && substr($path, 0, $base_path_length) === $base_path) {
      $path = substr($path, $base_path_length);
    }

    // Remove AJAX parameters.
    $path = preg_replace('/([?&])(ajax_form|_wrapper_format)=[^&#]+/', '$1', $path);
    // Sanitize empty GET parameter arrays.
    $path = preg_replace('/\?(#|$)/', '$1', $path);

    return $path;
  }

}
